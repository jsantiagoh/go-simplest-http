package main

import (
	_ "expvar"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

var start = time.Now()

func main() {
	http.HandleFunc("/env", environment)
	http.HandleFunc("/", root)

	log.Println("Listening at :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func root(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	fmt.Fprintf(w, "Running\nstart: %v\nnow: %v\nuptime: %v", start, now, now.Sub(start))
}

func environment(w http.ResponseWriter, r *http.Request) {
	for _, e := range os.Environ() {
		fmt.Fprintf(w, "- %v\n", e)
	}

}
